// Copyright 2018–2021, The libhandy-rs Project Developers.
// See the COPYRIGHT file at the top-level directory of this distribution.
// Licensed under the LGPL-2.1-only, see the LICENSE file or <https://opensource.org/licenses/LGPL-2.1>
#![cfg_attr(docsrs, feature(doc_cfg))]
#![allow(clippy::needless_doctest_main)]
#![doc(html_logo_url = "https://gitlab.gnome.org/GNOME/libhandy/-/raw/main/doc/libhandy.svg")]
//! # Rust Handy bindings
//!
//! This library contains safe Rust bindings for Handy, a library that offers
//! building blocks for modern adaptive GNOME applications.
//!
//! See also
//!
//! - [GTK 3 Rust bindings documentation](mod@gtk)
//! - [Libhandy documentation](https://gnome.pages.gitlab.gnome.org/libhandy/)
//! - [gtk-rs project overview](https://gtk-rs.org/)
//!
//! # Example
//!
//! ```no_run
//! use gtk::prelude::*;
//! use gtk::{Application, Box, ListBox, Orientation};
//! use libhandy::prelude::*;
//! use libhandy::{ActionRow, ApplicationWindow, HeaderBar};
//!
//! fn main() {
//!     let application = Application::builder()
//!         .application_id("com.example.FirstHandyApp")
//!         .build();
//!
//!     application.connect_activate(|app| {
//!         libhandy::init();
//!
//!         // ActionRows are only available in Handy
//!         let row = ActionRow::builder()
//!             .activatable(true)
//!             .selectable(false)
//!             .margin(32)
//!             .title("Click me")
//!             .build();
//!         row.connect_activated(|_| {
//!             eprintln!("Clicked!");
//!         });
//!
//!         let list = ListBox::builder().child(&row).build();
//!         // the content class makes the list look nicer
//!         list.style_context().add_class("content");
//!
//!         // Combine the content in a box
//!         let content = Box::new(Orientation::Vertical, 0);
//!         // Handy's ApplicationWindow does not include a HeaderBar
//!         content.add(
//!             &HeaderBar::builder()
//!                 .show_close_button(true)
//!                 .title("First Handy Program")
//!                 .build(),
//!         );
//!         content.add(&list);
//!
//!         let window = ApplicationWindow::builder()
//!             .default_width(350)
//!             .default_height(70)
//!             // add content to window
//!             .child(&content)
//!             .build();
//!         window.set_application(Some(app));
//!         window.show_all();
//!     });
//!
//!     application.run();
//! }
//! ```

#![cfg_attr(feature = "dox", feature(doc_cfg))]
#![allow(deprecated)]
#![allow(dead_code)]

/// Asserts that this is the main thread and `gtk::init` has been called.
macro_rules! assert_initialized_main_thread {
    () => {
        if !::gtk::is_initialized_main_thread() {
            if ::gtk::is_initialized() {
                panic!("Libhandy may only be used from the main thread.");
            } else {
                panic!("Gtk has to be initialized before using libhandy.");
            }
        }
    };
}

macro_rules! skip_assert_initialized {
    () => {};
}

#[doc(hidden)]
pub use gdk;
#[doc(hidden)]
pub use gio;
#[doc(hidden)]
pub use glib;
#[doc(hidden)]
pub use gtk;
#[doc(hidden)]
pub use pango;

#[allow(clippy::type_complexity)]
mod auto;
mod value_object;

#[macro_use]
pub mod subclass;
pub mod prelude;

pub use auto::*;

pub use auto::functions::*;
