// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT
#![allow(deprecated)]

use glib::{
    prelude::*,
    signal::{connect_raw, SignalHandlerId},
    translate::*,
};
use std::{boxed::Box as Box_, fmt, mem::transmute};

glib::wrapper! {
    #[doc(alias = "HdyAvatar")]
    pub struct Avatar(Object<ffi::HdyAvatar, ffi::HdyAvatarClass>) @extends gtk::DrawingArea, gtk::Widget, @implements gtk::Buildable;

    match fn {
        type_ => || ffi::hdy_avatar_get_type(),
    }
}

impl Avatar {
    #[doc(alias = "hdy_avatar_new")]
    pub fn new(size: i32, text: Option<&str>, show_initials: bool) -> Avatar {
        assert_initialized_main_thread!();
        unsafe {
            gtk::Widget::from_glib_none(ffi::hdy_avatar_new(
                size,
                text.to_glib_none().0,
                show_initials.into_glib(),
            ))
            .unsafe_cast()
        }
    }

    // rustdoc-stripper-ignore-next
    /// Creates a new builder-pattern struct instance to construct [`Avatar`] objects.
    ///
    /// This method returns an instance of [`AvatarBuilder`](crate::builders::AvatarBuilder) which can be used to create [`Avatar`] objects.
    pub fn builder() -> AvatarBuilder {
        AvatarBuilder::new()
    }

    #[cfg(feature = "v1_2")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_2")))]
    #[doc(alias = "hdy_avatar_draw_to_pixbuf")]
    pub fn draw_to_pixbuf(&self, size: i32, scale_factor: i32) -> Option<gdk_pixbuf::Pixbuf> {
        unsafe {
            from_glib_full(ffi::hdy_avatar_draw_to_pixbuf(
                self.to_glib_none().0,
                size,
                scale_factor,
            ))
        }
    }

    #[doc(alias = "hdy_avatar_get_icon_name")]
    #[doc(alias = "get_icon_name")]
    pub fn icon_name(&self) -> Option<glib::GString> {
        unsafe { from_glib_none(ffi::hdy_avatar_get_icon_name(self.to_glib_none().0)) }
    }

    #[cfg(feature = "v1_2")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_2")))]
    #[doc(alias = "hdy_avatar_get_loadable_icon")]
    #[doc(alias = "get_loadable_icon")]
    pub fn loadable_icon(&self) -> Option<gio::LoadableIcon> {
        unsafe { from_glib_none(ffi::hdy_avatar_get_loadable_icon(self.to_glib_none().0)) }
    }

    #[doc(alias = "hdy_avatar_get_show_initials")]
    #[doc(alias = "get_show_initials")]
    pub fn shows_initials(&self) -> bool {
        unsafe { from_glib(ffi::hdy_avatar_get_show_initials(self.to_glib_none().0)) }
    }

    #[doc(alias = "hdy_avatar_get_size")]
    #[doc(alias = "get_size")]
    pub fn size(&self) -> i32 {
        unsafe { ffi::hdy_avatar_get_size(self.to_glib_none().0) }
    }

    #[doc(alias = "hdy_avatar_get_text")]
    #[doc(alias = "get_text")]
    pub fn text(&self) -> Option<glib::GString> {
        unsafe { from_glib_none(ffi::hdy_avatar_get_text(self.to_glib_none().0)) }
    }

    #[doc(alias = "hdy_avatar_set_icon_name")]
    pub fn set_icon_name(&self, icon_name: Option<&str>) {
        unsafe {
            ffi::hdy_avatar_set_icon_name(self.to_glib_none().0, icon_name.to_glib_none().0);
        }
    }

    #[cfg_attr(feature = "v1_2", deprecated = "Since 1.2")]
    #[allow(deprecated)]
    #[doc(alias = "hdy_avatar_set_image_load_func")]
    pub fn set_image_load_func(
        &self,
        load_image: Option<Box_<dyn Fn(i32) -> Option<gdk_pixbuf::Pixbuf> + 'static>>,
    ) {
        let load_image_data: Box_<
            Option<Box_<dyn Fn(i32) -> Option<gdk_pixbuf::Pixbuf> + 'static>>,
        > = Box_::new(load_image);
        unsafe extern "C" fn load_image_func(
            size: libc::c_int,
            user_data: glib::ffi::gpointer,
        ) -> *mut gdk_pixbuf::ffi::GdkPixbuf {
            let callback: &Option<Box_<dyn Fn(i32) -> Option<gdk_pixbuf::Pixbuf> + 'static>> =
                &*(user_data as *mut _);
            if let Some(ref callback) = *callback {
                callback(size)
            } else {
                panic!("cannot get closure...")
            }
            .to_glib_full()
        }
        let load_image = if load_image_data.is_some() {
            Some(load_image_func as _)
        } else {
            None
        };
        unsafe extern "C" fn destroy_func(data: glib::ffi::gpointer) {
            let _callback: Box_<Option<Box_<dyn Fn(i32) -> Option<gdk_pixbuf::Pixbuf> + 'static>>> =
                Box_::from_raw(data as *mut _);
        }
        let destroy_call3 = Some(destroy_func as _);
        let super_callback0: Box_<
            Option<Box_<dyn Fn(i32) -> Option<gdk_pixbuf::Pixbuf> + 'static>>,
        > = load_image_data;
        unsafe {
            ffi::hdy_avatar_set_image_load_func(
                self.to_glib_none().0,
                load_image,
                Box_::into_raw(super_callback0) as *mut _,
                destroy_call3,
            );
        }
    }

    #[cfg(feature = "v1_2")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_2")))]
    #[doc(alias = "hdy_avatar_set_loadable_icon")]
    pub fn set_loadable_icon(&self, icon: Option<&impl IsA<gio::LoadableIcon>>) {
        unsafe {
            ffi::hdy_avatar_set_loadable_icon(
                self.to_glib_none().0,
                icon.map(|p| p.as_ref()).to_glib_none().0,
            );
        }
    }

    #[doc(alias = "hdy_avatar_set_show_initials")]
    pub fn set_show_initials(&self, show_initials: bool) {
        unsafe {
            ffi::hdy_avatar_set_show_initials(self.to_glib_none().0, show_initials.into_glib());
        }
    }

    #[doc(alias = "hdy_avatar_set_size")]
    pub fn set_size(&self, size: i32) {
        unsafe {
            ffi::hdy_avatar_set_size(self.to_glib_none().0, size);
        }
    }

    #[doc(alias = "hdy_avatar_set_text")]
    pub fn set_text(&self, text: Option<&str>) {
        unsafe {
            ffi::hdy_avatar_set_text(self.to_glib_none().0, text.to_glib_none().0);
        }
    }

    #[doc(alias = "icon-name")]
    pub fn connect_icon_name_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_icon_name_trampoline<F: Fn(&Avatar) + 'static>(
            this: *mut ffi::HdyAvatar,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::icon-name\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    notify_icon_name_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[cfg(feature = "v1_2")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_2")))]
    #[doc(alias = "loadable-icon")]
    pub fn connect_loadable_icon_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_loadable_icon_trampoline<F: Fn(&Avatar) + 'static>(
            this: *mut ffi::HdyAvatar,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::loadable-icon\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    notify_loadable_icon_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[doc(alias = "show-initials")]
    pub fn connect_show_initials_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_show_initials_trampoline<F: Fn(&Avatar) + 'static>(
            this: *mut ffi::HdyAvatar,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::show-initials\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    notify_show_initials_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[doc(alias = "size")]
    pub fn connect_size_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_size_trampoline<F: Fn(&Avatar) + 'static>(
            this: *mut ffi::HdyAvatar,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::size\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    notify_size_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }

    #[doc(alias = "text")]
    pub fn connect_text_notify<F: Fn(&Self) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn notify_text_trampoline<F: Fn(&Avatar) + 'static>(
            this: *mut ffi::HdyAvatar,
            _param_spec: glib::ffi::gpointer,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(&from_glib_borrow(this))
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"notify::text\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    notify_text_trampoline::<F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }
}

impl Default for Avatar {
    fn default() -> Self {
        glib::object::Object::new::<Self>()
    }
}

// rustdoc-stripper-ignore-next
/// A [builder-pattern] type to construct [`Avatar`] objects.
///
/// [builder-pattern]: https://doc.rust-lang.org/1.0.0/style/ownership/builders.html
#[must_use = "The builder must be built to be used"]
pub struct AvatarBuilder {
    builder: glib::object::ObjectBuilder<'static, Avatar>,
}

impl AvatarBuilder {
    fn new() -> Self {
        Self {
            builder: glib::object::Object::builder(),
        }
    }

    pub fn icon_name(self, icon_name: impl Into<glib::GString>) -> Self {
        Self {
            builder: self.builder.property("icon-name", icon_name.into()),
        }
    }

    #[cfg(feature = "v1_2")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v1_2")))]
    pub fn loadable_icon(self, loadable_icon: &impl IsA<gio::LoadableIcon>) -> Self {
        Self {
            builder: self
                .builder
                .property("loadable-icon", loadable_icon.clone().upcast()),
        }
    }

    pub fn show_initials(self, show_initials: bool) -> Self {
        Self {
            builder: self.builder.property("show-initials", show_initials),
        }
    }

    pub fn size(self, size: i32) -> Self {
        Self {
            builder: self.builder.property("size", size),
        }
    }

    pub fn text(self, text: impl Into<glib::GString>) -> Self {
        Self {
            builder: self.builder.property("text", text.into()),
        }
    }

    pub fn app_paintable(self, app_paintable: bool) -> Self {
        Self {
            builder: self.builder.property("app-paintable", app_paintable),
        }
    }

    pub fn can_default(self, can_default: bool) -> Self {
        Self {
            builder: self.builder.property("can-default", can_default),
        }
    }

    pub fn can_focus(self, can_focus: bool) -> Self {
        Self {
            builder: self.builder.property("can-focus", can_focus),
        }
    }

    pub fn events(self, events: gdk::EventMask) -> Self {
        Self {
            builder: self.builder.property("events", events),
        }
    }

    pub fn expand(self, expand: bool) -> Self {
        Self {
            builder: self.builder.property("expand", expand),
        }
    }

    pub fn focus_on_click(self, focus_on_click: bool) -> Self {
        Self {
            builder: self.builder.property("focus-on-click", focus_on_click),
        }
    }

    pub fn halign(self, halign: gtk::Align) -> Self {
        Self {
            builder: self.builder.property("halign", halign),
        }
    }

    pub fn has_default(self, has_default: bool) -> Self {
        Self {
            builder: self.builder.property("has-default", has_default),
        }
    }

    pub fn has_focus(self, has_focus: bool) -> Self {
        Self {
            builder: self.builder.property("has-focus", has_focus),
        }
    }

    pub fn has_tooltip(self, has_tooltip: bool) -> Self {
        Self {
            builder: self.builder.property("has-tooltip", has_tooltip),
        }
    }

    pub fn height_request(self, height_request: i32) -> Self {
        Self {
            builder: self.builder.property("height-request", height_request),
        }
    }

    pub fn hexpand(self, hexpand: bool) -> Self {
        Self {
            builder: self.builder.property("hexpand", hexpand),
        }
    }

    pub fn hexpand_set(self, hexpand_set: bool) -> Self {
        Self {
            builder: self.builder.property("hexpand-set", hexpand_set),
        }
    }

    pub fn is_focus(self, is_focus: bool) -> Self {
        Self {
            builder: self.builder.property("is-focus", is_focus),
        }
    }

    pub fn margin(self, margin: i32) -> Self {
        Self {
            builder: self.builder.property("margin", margin),
        }
    }

    pub fn margin_bottom(self, margin_bottom: i32) -> Self {
        Self {
            builder: self.builder.property("margin-bottom", margin_bottom),
        }
    }

    pub fn margin_end(self, margin_end: i32) -> Self {
        Self {
            builder: self.builder.property("margin-end", margin_end),
        }
    }

    pub fn margin_start(self, margin_start: i32) -> Self {
        Self {
            builder: self.builder.property("margin-start", margin_start),
        }
    }

    pub fn margin_top(self, margin_top: i32) -> Self {
        Self {
            builder: self.builder.property("margin-top", margin_top),
        }
    }

    pub fn name(self, name: impl Into<glib::GString>) -> Self {
        Self {
            builder: self.builder.property("name", name.into()),
        }
    }

    pub fn no_show_all(self, no_show_all: bool) -> Self {
        Self {
            builder: self.builder.property("no-show-all", no_show_all),
        }
    }

    pub fn opacity(self, opacity: f64) -> Self {
        Self {
            builder: self.builder.property("opacity", opacity),
        }
    }

    pub fn parent(self, parent: &impl IsA<gtk::Container>) -> Self {
        Self {
            builder: self.builder.property("parent", parent.clone().upcast()),
        }
    }

    pub fn receives_default(self, receives_default: bool) -> Self {
        Self {
            builder: self.builder.property("receives-default", receives_default),
        }
    }

    pub fn sensitive(self, sensitive: bool) -> Self {
        Self {
            builder: self.builder.property("sensitive", sensitive),
        }
    }

    //pub fn style(self, style: &impl IsA</*Ignored*/gtk::Style>) -> Self {
    //    Self { builder: self.builder.property("style", style.clone().upcast()), }
    //}

    pub fn tooltip_markup(self, tooltip_markup: impl Into<glib::GString>) -> Self {
        Self {
            builder: self
                .builder
                .property("tooltip-markup", tooltip_markup.into()),
        }
    }

    pub fn tooltip_text(self, tooltip_text: impl Into<glib::GString>) -> Self {
        Self {
            builder: self.builder.property("tooltip-text", tooltip_text.into()),
        }
    }

    pub fn valign(self, valign: gtk::Align) -> Self {
        Self {
            builder: self.builder.property("valign", valign),
        }
    }

    pub fn vexpand(self, vexpand: bool) -> Self {
        Self {
            builder: self.builder.property("vexpand", vexpand),
        }
    }

    pub fn vexpand_set(self, vexpand_set: bool) -> Self {
        Self {
            builder: self.builder.property("vexpand-set", vexpand_set),
        }
    }

    pub fn visible(self, visible: bool) -> Self {
        Self {
            builder: self.builder.property("visible", visible),
        }
    }

    pub fn width_request(self, width_request: i32) -> Self {
        Self {
            builder: self.builder.property("width-request", width_request),
        }
    }

    // rustdoc-stripper-ignore-next
    /// Build the [`Avatar`].
    #[must_use = "Building the object from the builder is usually expensive and is not expected to have side effects"]
    pub fn build(self) -> Avatar {
        self.builder.build()
    }
}

impl fmt::Display for Avatar {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("Avatar")
    }
}
