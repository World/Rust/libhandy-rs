// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

use crate::{NavigationDirection, SwipeTracker};
use glib::{
    prelude::*,
    signal::{connect_raw, SignalHandlerId},
    translate::*,
};
use std::{boxed::Box as Box_, fmt, mem, mem::transmute};

glib::wrapper! {
    #[doc(alias = "HdySwipeable")]
    pub struct Swipeable(Interface<ffi::HdySwipeable, ffi::HdySwipeableInterface>) @requires gtk::Widget, gtk::Buildable;

    match fn {
        type_ => || ffi::hdy_swipeable_get_type(),
    }
}

impl Swipeable {
    pub const NONE: Option<&'static Swipeable> = None;
}

mod sealed {
    pub trait Sealed {}
    impl<T: super::IsA<super::Swipeable>> Sealed for T {}
}

pub trait SwipeableExt: IsA<Swipeable> + sealed::Sealed + 'static {
    #[doc(alias = "hdy_swipeable_emit_child_switched")]
    fn emit_child_switched(&self, index: u32, duration: i64) {
        unsafe {
            ffi::hdy_swipeable_emit_child_switched(self.as_ref().to_glib_none().0, index, duration);
        }
    }

    #[doc(alias = "hdy_swipeable_get_cancel_progress")]
    #[doc(alias = "get_cancel_progress")]
    fn cancel_progress(&self) -> f64 {
        unsafe { ffi::hdy_swipeable_get_cancel_progress(self.as_ref().to_glib_none().0) }
    }

    #[doc(alias = "hdy_swipeable_get_distance")]
    #[doc(alias = "get_distance")]
    fn distance(&self) -> f64 {
        unsafe { ffi::hdy_swipeable_get_distance(self.as_ref().to_glib_none().0) }
    }

    #[doc(alias = "hdy_swipeable_get_progress")]
    #[doc(alias = "get_progress")]
    fn progress(&self) -> f64 {
        unsafe { ffi::hdy_swipeable_get_progress(self.as_ref().to_glib_none().0) }
    }

    #[doc(alias = "hdy_swipeable_get_snap_points")]
    #[doc(alias = "get_snap_points")]
    fn snap_points(&self) -> Vec<f64> {
        unsafe {
            let mut n_snap_points = mem::MaybeUninit::uninit();
            let ret = FromGlibContainer::from_glib_full_num(
                ffi::hdy_swipeable_get_snap_points(
                    self.as_ref().to_glib_none().0,
                    n_snap_points.as_mut_ptr(),
                ),
                n_snap_points.assume_init() as _,
            );
            ret
        }
    }

    #[doc(alias = "hdy_swipeable_get_swipe_area")]
    #[doc(alias = "get_swipe_area")]
    fn swipe_area(
        &self,
        navigation_direction: NavigationDirection,
        is_drag: bool,
    ) -> gdk::Rectangle {
        unsafe {
            let mut rect = gdk::Rectangle::uninitialized();
            ffi::hdy_swipeable_get_swipe_area(
                self.as_ref().to_glib_none().0,
                navigation_direction.into_glib(),
                is_drag.into_glib(),
                rect.to_glib_none_mut().0,
            );
            rect
        }
    }

    #[doc(alias = "hdy_swipeable_get_swipe_tracker")]
    #[doc(alias = "get_swipe_tracker")]
    fn swipe_tracker(&self) -> Option<SwipeTracker> {
        unsafe {
            from_glib_none(ffi::hdy_swipeable_get_swipe_tracker(
                self.as_ref().to_glib_none().0,
            ))
        }
    }

    #[doc(alias = "hdy_swipeable_switch_child")]
    fn switch_child(&self, index: u32, duration: i64) {
        unsafe {
            ffi::hdy_swipeable_switch_child(self.as_ref().to_glib_none().0, index, duration);
        }
    }

    #[doc(alias = "child-switched")]
    fn connect_child_switched<F: Fn(&Self, u32, i64) + 'static>(&self, f: F) -> SignalHandlerId {
        unsafe extern "C" fn child_switched_trampoline<
            P: IsA<Swipeable>,
            F: Fn(&P, u32, i64) + 'static,
        >(
            this: *mut ffi::HdySwipeable,
            index: libc::c_uint,
            duration: i64,
            f: glib::ffi::gpointer,
        ) {
            let f: &F = &*(f as *const F);
            f(
                Swipeable::from_glib_borrow(this).unsafe_cast_ref(),
                index,
                duration,
            )
        }
        unsafe {
            let f: Box_<F> = Box_::new(f);
            connect_raw(
                self.as_ptr() as *mut _,
                b"child-switched\0".as_ptr() as *const _,
                Some(transmute::<_, unsafe extern "C" fn()>(
                    child_switched_trampoline::<Self, F> as *const (),
                )),
                Box_::into_raw(f),
            )
        }
    }
}

impl<O: IsA<Swipeable>> SwipeableExt for O {}

impl fmt::Display for Swipeable {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("Swipeable")
    }
}
