use crate::ApplicationWindow;
use gtk::subclass::prelude::*;

pub trait HdyApplicationWindowImpl: ApplicationWindowImpl + 'static {}

unsafe impl<T: HdyApplicationWindowImpl> IsSubclassable<T> for ApplicationWindow {}
