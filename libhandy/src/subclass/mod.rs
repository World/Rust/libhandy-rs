pub mod application_window;

pub mod prelude {
    pub use super::application_window::HdyApplicationWindowImpl;
    pub use gio::subclass::prelude::*;
}
