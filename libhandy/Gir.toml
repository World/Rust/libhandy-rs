[external_libraries]
Gio = {min_version = "2.44"}
Gtk = {min_version = "3.24"} # we already enable v3_24 feature by default

[options]
girs_directories = ["../", "../gir-files"]
library = "Handy"
version = "1"
min_cfg_version = "1.0"
target_path = "."
work_mode = "normal"
generate_safety_asserts = true
deprecate_by_min_version = true
# with this option enabled, versions for gir and gir-files saved only to one file to minimize noise
single_version_file = true
generate_builder = true
use_gi_docgen = true

generate = [
    "Handy.CenteringPolicy",
    "Handy.ColorScheme",
    "Handy.DeckTransitionType",
    "Handy.EnumValueObject",
    "Handy.FlapFoldPolicy",
    "Handy.FlapTransitionType",
    "Handy.HeaderGroupChild",
    "Handy.HeaderGroupChildType",
    "Handy.LeafletTransitionType",
    "Handy.NavigationDirection",
    "Handy.SqueezerTransitionType",
    "Handy.SwipeGroup",
    "Handy.Swipeable",
    "Handy.ViewSwitcherPolicy",
    "Handy.Window",
    "Handy.WindowHandle",
    "Handy.ActionRow",
    "Handy.ApplicationWindow",
    "Handy.Carousel",
    "Handy.CarouselIndicatorDots",
    "Handy.CarouselIndicatorLines",
    "Handy.Clamp",
    "Handy.ComboRow",
    "Handy.ExpanderRow",
    "Handy.Flap",
    "Handy.HeaderGroup",
    "Handy.Keypad",
    "Handy.PreferencesGroup",
    "Handy.PreferencesPage",
    "Handy.PreferencesRow",
    "Handy.PreferencesWindow",
    "Handy.Squeezer",
    "Handy.StatusPage",
    "Handy.SwipeTracker",
    "Handy.TabBar",
    "Handy.TabPage",
    "Handy.TabView",
    "Handy.TitleBar",
    "Handy.ViewSwitcher",
    "Handy.ViewSwitcherBar",
    "Handy.ViewSwitcherTitle",
]

manual = [
    "GLib.DestroyNotify",
    "GLib.String",
    "GLib.Variant",
    "GObject.Object",
    "GObject.Value",
    "Gdk.Display",
    "Gdk.DragContext",
    "Gdk.Event",
    "Gdk.EventMask",
    "Gdk.Rectangle",
    "GdkPixbuf.Pixbuf",
    "Gio.ActionGroup",
    "Gio.ActionMap",
    "Gio.AsyncReadyCallback",
    "Gio.AsyncResult",
    "Gio.Cancellable",
    "Gio.Icon",
    "Gio.ListModel",
    "Gio.LoadableIcon",
    "Gio.MenuModel",
    "Gtk.Actionable",
    "Gtk.Adjustment",
    "Gtk.Align",
    "Gtk.ApplicationWindow",
    "Gtk.BaselinePosition",
    "Gtk.Bin",
    "Gtk.Box",
    "Gtk.Buildable",
    "Gtk.Button",
    "Gtk.Container",
    "Gtk.CornerType",
    "Gtk.Dialog",
    "Gtk.DrawingArea",
    "Gtk.Entry",
    "Gtk.EventBox",
    "Gtk.Grid",
    "Gtk.HeaderBar",
    "Gtk.IconSize",
    "Gtk.ListBoxRow",
    "Gtk.Orientable",
    "Gtk.Orientation",
    "Gtk.PackType",
    "Gtk.PolicyType",
    "Gtk.PositionType",
    "Gtk.ReliefStyle",
    "Gtk.ResizeMode",
    "Gtk.ScrolledWindow",
    "Gtk.SelectionData",
    "Gtk.ShadowType",
    "Gtk.Stack",
    "Gtk.TargetList",
    "Gtk.Widget",
    "Pango.EllipsizeMode",
]

[[object]]
name = "Handy.*"
status = "generate"
    [[object.function]]
    name = "string_utf8_truncate"
    ignore = true
    [[object.function]]
    name = "string_utf8_len"
    ignore = true

[[object]]
name = "Handy.Avatar"
status = "generate"
    [[object.function]]
    name = "draw_to_pixbuf_async"
    # gir panics if this is generated
    ignore = true

[[object]]
name = "Handy.Deck"
status = "generate"
    [[object.child_prop]]
    name = "name"
    type = "utf8"

[[object]]
name = "Handy.HeaderBar"
status = "generate"
trait_name = "HdyHeaderBarExt"

[[object]]
name = "Handy.Leaflet"
child_type = "Gtk.Widget"
status = "generate"
    [[object.child_prop]]
    name = "icon-name"
    type = "utf8"
    [[object.child_prop]]
    name = "name"
    type = "utf8"
    [[object.child_prop]]
    name = "needs-attention"
    type = "gboolean"
    [[object.child_prop]]
    name = "position"
    type = "gint"
    [[object.child_prop]]
    name = "title"
    type = "utf8"

[[object]]
name = "Handy.SearchBar"
trait_name = "HdySearchBarExt"
status = "generate"
    [[object.function]]
    name = "handle_event"
        [[object.function.parameter]]
        name = "event"
        const = true

[[object]]
name = "Handy.StyleManager"
status = "generate"
version = "1.6"
generate_builder = false
    [[object.function]]
    name = "get_system_supports_color_schemes"
    rename = "system_supports_color_schemes"

[[object]]
name = "Handy.ValueObject"
status = "generate"
    [[object.function]]
    name = "dup_string"
    ignore = true
    [[object.function]]
    name = "copy_value"
    ignore = true
    [[object.function]]
    name = "new_take_string"
    ignore = true
    [[object.function]]
    name = "new_string"
    ignore = true
    [[object.property]]
    name = "value"
    ignore = true

[[object]]
name = "Gtk.Window"
status = "manual"
trait_name = "GtkWindowExt"
