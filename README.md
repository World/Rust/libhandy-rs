# libhandy-rs

**Note**: libhandy-rs is no longer actively maintained as well as other gtk3-rs crates.

This repository contains the rust bindings for libhandy.

- [Documentation](https://world.pages.gitlab.gnome.org/Rust/libhandy-rs)

## Using

Add this line to your Cargo file

```
[dependencies]
libhandy = { git = "https://gitlab.gnome.org/World/Rust/libhandy-rs" }
```

## Build

We use [gir](https://github.com/gtk-rs/gir) to generate rust libhandy bindings. The bindings are split in two parts, sys and api.

```shell
git clone --recurse https://gitlab.gnome.org/World/Rust/libhandy-rs.git
./generator.py
```
